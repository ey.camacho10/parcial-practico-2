package it.feio.android.omninotes;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;

import it.feio.android.omninotes.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityLocationVisibleTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityLocationVisibleTest() {
        ViewInteraction viewInteraction = onView(
                allOf(withId(it.feio.android.omninotes.foss.R.id.fab_expand_menu_button),
                        withParent(withId(it.feio.android.omninotes.foss.R.id.fab)),
                        isDisplayed()));
        viewInteraction.perform(click());

        ViewInteraction floatingActionButton = onView(
                allOf(withId(it.feio.android.omninotes.foss.R.id.fab_note),
                        withParent(withId(it.feio.android.omninotes.foss.R.id.fab)),
                        isDisplayed()));
        floatingActionButton.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction editText = onView(
                allOf(withId(it.feio.android.omninotes.foss.R.id.detail_title),
                        withParent(allOf(withId(it.feio.android.omninotes.foss.R.id.title_wrapper),
                                withParent(withId(it.feio.android.omninotes.foss.R.id.detail_tile_card)))),
                        isDisplayed()));
        editText.perform(click());

        ViewInteraction editText2 = onView(
                allOf(withId(it.feio.android.omninotes.foss.R.id.detail_title),
                        withParent(allOf(withId(it.feio.android.omninotes.foss.R.id.title_wrapper),
                                withParent(withId(it.feio.android.omninotes.foss.R.id.detail_tile_card)))),
                        isDisplayed()));
        editText2.perform(replaceText("D"), closeSoftKeyboard());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction editText3 = onView(
                allOf(withId(it.feio.android.omninotes.foss.R.id.detail_title), withText("D"),
                        withParent(allOf(withId(it.feio.android.omninotes.foss.R.id.title_wrapper),
                                withParent(withId(it.feio.android.omninotes.foss.R.id.detail_tile_card)))),
                        isDisplayed()));
        editText3.perform(replaceText("Dd"), closeSoftKeyboard());

        ViewInteraction editText4 = onView(
                withId(it.feio.android.omninotes.foss.R.id.detail_content));
        editText4.perform(scrollTo(), replaceText("Dd"), closeSoftKeyboard());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction editText5 = onView(
                allOf(withId(it.feio.android.omninotes.foss.R.id.detail_content), withText("Dd")));
        editText5.perform(scrollTo(), replaceText("Ddd"), closeSoftKeyboard());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(40000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction imageButton = onView(
                allOf(withContentDescription("drawer open"),
                        withParent(withId(it.feio.android.omninotes.foss.R.id.toolbar)),
                        isDisplayed()));
        imageButton.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(150);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction frameLayout = onView(
                allOf(withId(it.feio.android.omninotes.foss.R.id.root),
                        childAtPosition(
                                withId(it.feio.android.omninotes.foss.R.id.list),
                                0),
                        isDisplayed()));
        frameLayout.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(it.feio.android.omninotes.foss.R.id.creation),
                        withParent(allOf(withId(it.feio.android.omninotes.foss.R.id.detail_timestamps),
                                withParent(withId(it.feio.android.omninotes.foss.R.id.detail_wrapper)))),
                        isDisplayed()));
        textView.perform(replaceText("Created: moments ago"), closeSoftKeyboard());

        ViewInteraction textView2 = onView(
                allOf(withId(it.feio.android.omninotes.foss.R.id.last_modification),
                        withParent(allOf(withId(it.feio.android.omninotes.foss.R.id.detail_timestamps),
                                withParent(withId(it.feio.android.omninotes.foss.R.id.detail_wrapper)))),
                        isDisplayed()));
        textView2.perform(replaceText("Updated: moments ago"), closeSoftKeyboard());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(80000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(it.feio.android.omninotes.foss.R.id.menu_attachment), withContentDescription("Attachment"), isDisplayed()));
        actionMenuItemView.perform(click());

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
